# ![](./img/logo.png) StalkerParty

Demonio creado usando iNotify, para el seguimiento de uno o varios directorios, en una máquina cliente desde una máquina servidor, que recibirá iformación acerca del estado de los mismos.

## Descripción de la tarea

> El objetivo es que el demonio escriba en un log todos los eventos que se produzcan en la monitorización de uno o varios directorios.

> El **demonio** debería:

> * Lanzarse y detenerse correctamente.
> * Hacer el seguimiento de una o varias carpetas que se le pasen como parámetro.
> * Mostrar como mínimo todos los eventos se produzcan correspondientes al primer bloque en la tabla colgada en el campus virtual ***"EventosInotify"*** ( ***IN_ACCESS - IN_OPEN*** ).
>> * **Opcional**: No volver a lanzar el demonio si ya está activo.
>> * **Opcional**: Monitorizar los eventos en todos los subdirectorios de los directorios especificados.
>> * **Opcional**: Si se añaden o borran subdirectorios la monitorización debería actualizarse dinámicamente.

> * El **demonio** debe enviar un resumen de las estadísticas cada cierto tiempo a un **servidor**.
> * El servidor muestra por pantalla o almacena en un fichero dicha información.
>>  *  **Opcional**: Comunicación bidireccional entre el servidor y el cliente.
>>  *  **Opcional**: Varios procesos o demonios cliente que envían información a un servidor.
>>  *  **Opcional**: Convertir en un demonio el código del servidor.
